# Tekton CI/CD Configuration

## Setup
> This setup tutorial will be using tekton pipeline to be CI/CD on MR changes in Gitlab. This pipeline will be running locally using minikube and can be seen using Tekton Dashboard

1. [Optional] Make setup pristine
- [Warning], this will remove all instances of resources in local minikube
```sh
minikube stop

minikube delete

minikube start
```

2. Setup Tekton Pipeline
- This will install tekton pipeline 
```sh
# install tekton pipeline
kubectl apply --filename \
https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml

kubectl get pods --namespace tekton-pipelines --watch # watch until all statuses are 1/1 
```

3. Setup Tekton Dashboard
- This will install tekton dashboard and run it locally
```sh
kubectl apply --filename \
https://storage.googleapis.com/tekton-releases/dashboard/latest/release-full.yaml

kubectl get pods --namespace tekton-pipelines --watch # watch until all statuses are 1/1 

kubectl port-forward -n tekton-pipelines service/tekton-dashboard 9097:9097
```

4. Create Secret
- This step will create all secrets and tokens necessary for running this pipeline

    4.a. create git secret
    ```sh
    cat ~/.ssh/id_rsa | base64 > id_rsa.base64
    ```

    4.b. create secret docker hub
    ```sh
    echo -n <USERNAME>:<PASSWORD> | base64 > dockerhub.base64
    ```

    4.c. create PAT gitlab
    - go to https://gitlab.com/-/profile/personal_access_tokens 
    - create personal access tokens and store it

    4.d. generate random token for gitlab webhook
    ```sh
    openssl rand -base64 12 > gitlab-token-secret
    ```

5. Fill Secret
- fill secret on appropriate field on `misc/secret.yaml`

6. Prepare the repository
- Here is example of the repository that can be used by forking it 
- https://gitlab.com/itn.malik.rafsanjani/tekton-example

7. Apply Resource
- This will create tasks, pipelines, and secrets resources that can be seen on tekton dashboard
    ```sh
    kubectl apply -f tasks

    kubectl apply -f pipelines

    kubectl apply -f misc
    ```

8. Setup Trigger
- This will install tekton trigger and create gitlab trigger resource 
    ```sh
    kubectl apply --filename \
    https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
    kubectl apply --filename \
    https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml

    kubectl get pods --namespace tekton-pipelines --watch # watch until all statuses are 1/1 

    kubectl apply -f triggers
    ```

9. Try trigger
- This example is based on (https://github.com/WLun001/cncf-demo/tree/main/examples/hello-world/tekton/trigger)
- Setup port forwarding
    ```sh
    kubectl port-forward service/el-gitlab-listener 8080
    ```

- Setup tunneling
    ```sh
    ngrok http 8080
    ```

- Setup webhook on Gitlab
    - go to (`<gitlab-repo-url>/-/hooks`) and add new webhook
    ![Webhook Setup](./imgs/Webhook-Setup.png)
    - fill url using url tunneling and secret token from previous random generated string
    - Check for merge request and comments

- Create Merge Request and add new commit there
    - check on tekton dashboard whether the pipeline is running or not

- You can also try to auto update manifest on another repository (https://gitlab.com/itn.malik.rafsanjani/tekton-manifest) by adding comment on MR
    - example: `deploy staging <hash-signature-image>`

